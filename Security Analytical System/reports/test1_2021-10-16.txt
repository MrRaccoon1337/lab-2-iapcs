Object security assessment report on 2021-10-16 04:26:39
Expert Name: test1




Single (Occur once and reoccur only after full review of made decisions)
2
Requried (Occur during improvement of means and measures)
6
Pereodic
7

Assessment of level of protection against NSA
Server Specialization (Existance of database servers, app servers, access servers, post servers and web-servers)
2
Minimization of quantity of copying devices on external media inside workstations (changeable HDDs, optical drives, printers, etc.)
6
Use of routers and firewalls
7
Use of software authentication means (smart-cards, biometric methods, etc.)
2
Use of technical protection means against side EM emitments
1

Assesement of level of protection against data integrity vulnerabilities
Existance of UPS
2
Existance of means for backup and monitoring of key subsystems
1
Existance of shielding means inside the ITS
2

Assessment of protection level of network OSs and their configs
Protection against NSA
Existance of resource access control system
2
Existance of user authentication system
6
Existance of system audit procedure
7
Integrity protection
Existance of file restoration means
2
Existance of file deleting percautions
1
Existance of OS protection means against corruption
2

Assessment of protection level of DBMS
Existnace of entry and password encryption subsystem
1
Existane of data access delimitation subsystem
2
Existance of query logging subsystem
2
Functioning in "client-server" mode without registration on server
1
Existance of data restoring mechanisms
2
Existance of transaction block mechanisms
0
Existance of DB impact permissions delimitation subsystem
0
Existance of DB testing tools
2
Existance of error informing subsystem
1
Existance of logging and DB alter control subsytems
2

Integrity protection
Existance of user authentication system
2
Existance of permission check for branches and interface objects subsystem
6
Existance of user work logs
7
Existance of access delimation to documents originals
2
Existance of permission check during document processing and printing
1
Existnace of email encryption subsystem
2

Protection against NSA
Existnace of limitation for data recources deleting/editing subsytem
1
Existance of correct data input subsystem
2
Existance of input data authorization subsystem
2
Existance of input data identification subsystem
1
