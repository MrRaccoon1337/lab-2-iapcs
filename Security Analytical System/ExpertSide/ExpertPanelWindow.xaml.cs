﻿using Security_Analytical_System.Classes;
using Security_Analytical_System.ExpertSide.SafetyMeansAndMeasures.OrganizationalMeasures;
using Security_Analytical_System.ExpertSide.SafetyMeansAndMeasures.SoftwareAndHardwareMeasuresWindow;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Security_Analytical_System.ExpertSide
{
    /// <summary>
    /// Логика взаимодействия для ExpertPanelWindow.xaml
    /// </summary>
    public partial class ExpertPanelWindow : Window
    {
        static string conString;
        Expert expert;
        string fileName;

        public ExpertPanelWindow(string con, Expert expert)
        {
            InitializeComponent();
            conString = con;
            this.expert = expert;
            fileName = $@"{Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName}\reports\{expert.username}_{DateTime.Now.ToString("yyyy-M-dd")}.txt";
        }

        private void btnOrganizationalMeasures_Click(object sender, RoutedEventArgs e)
        {
            OrganizationalMeasuresWindow organizationalMeasuresWidnow = new OrganizationalMeasuresWindow(conString, expert);
            organizationalMeasuresWidnow.Owner = this;
            Application.Current.MainWindow = organizationalMeasuresWidnow;
            this.Hide();
            organizationalMeasuresWidnow.Show();
        }

        private void btnSoftwareAndHardwareMeasures_Click(object sender, RoutedEventArgs e)
        {
            SoftwareAndHardwareMeasuresWindow softwareAndHardwareMeasuresWindow = new SoftwareAndHardwareMeasuresWindow(conString, expert);
            softwareAndHardwareMeasuresWindow.Owner = this;
            Application.Current.MainWindow = softwareAndHardwareMeasuresWindow;
            this.Hide();
            softwareAndHardwareMeasuresWindow.Show();
        }

        private void btnConclusion_Click(object sender, RoutedEventArgs e)
        {
            ConclusionWindow conclusionWindow = new ConclusionWindow(conString, expert);
            conclusionWindow.Owner = this;
            Application.Current.MainWindow = conclusionWindow;
            this.Hide();
            conclusionWindow.Show();
        }

        private void btnCreateReport_Click(object sender, RoutedEventArgs e)
        {
            List<string> tables = new List<string>
            {
                "organizational_measures",
                "hardware_measures",
                "system_measures",
                "application_measures"
            };
            Directory.CreateDirectory($@"{Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName}\reports");

            FileInfo fi = new FileInfo(fileName);


            List<Criteria> criterias = new List<Criteria>();
            List<Mark> marks = new List<Mark>();
            ConclusionMark result = HelperClass.LoadExpertConclusionMark(conString, expert.username);

            foreach (string table in tables)
            {
                criterias.AddRange(HelperClass.LoadCriterias(conString, table));
                marks.AddRange(HelperClass.LoadExpertMarks(conString, expert.id, table + "_marks"));
            }

            List<string> cats = criterias.Select(o => o.criteria_cat).Distinct().ToList();
            List<string> subcats = criterias.Select(o => o.criteria_subcat).Distinct().ToList();

            int cat_id = 0;
            int subcat_id = 0;

            try
            {
                if (fi.Exists)
                    fi.Delete();
                
                using (StreamWriter sw = fi.CreateText())
                {
                    
                    sw.WriteLine($"Object security assessment report on {DateTime.Now.ToString("yyyy-M-dd hh:mm:ss")}");
                    sw.WriteLine($"Expert Name: {expert.username}\n");
                    
                    foreach(Criteria criteria in criterias)
                    {
                        if (cat_id < cats.Count)
                            if (criteria.criteria_cat == cats[cat_id])
                            {
                                sw.WriteLine("\n" + criteria.criteria_cat);
                                cat_id++;
                            }

                        if (subcat_id < subcats.Count)
                            if (criteria.criteria_subcat == subcats[subcat_id])
                            {
                                sw.WriteLine(criteria.criteria_subcat);
                                subcat_id++;
                            }

                        sw.WriteLine(criteria.criteria_name + (!String.IsNullOrEmpty(criteria.criteria_desc) ? $" ({criteria.criteria_desc})" : ""));
                        sw.WriteLine(marks.FirstOrDefault(item => item.criteria_id == criteria.id).mark.ToString());
                    }
                }
                MessageBox.Show("Report Successfully Created!");
                btnCreateReport.IsEnabled = false;
                btnPrintReport.IsEnabled = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"App encountered next problem: {ex.Message}");
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.MainWindow = Owner;
            this.Hide();
            ((ExpertLoginWindow)Application.Current.MainWindow).txtBoxName.Clear();
            ((ExpertLoginWindow)Application.Current.MainWindow).passBoxPass.Clear();
            Owner.Show();
        }

        private void btnPrintReport_Click(object sender, RoutedEventArgs e)
        {
            string[] text = File.ReadAllLines(fileName);

            PrintDialog printDialog = new PrintDialog();
            if ((bool)printDialog.ShowDialog().GetValueOrDefault())
            {
                FlowDocument flowDocument = new FlowDocument();
                foreach (string line in text)
                {
                    Paragraph myParagraph = new Paragraph();
                    myParagraph.Margin = new Thickness(0);
                    myParagraph.Inlines.Add(new Run(line));
                    flowDocument.Blocks.Add(myParagraph);
                }
                DocumentPaginator paginator = ((IDocumentPaginatorSource)flowDocument).DocumentPaginator;
                printDialog.PrintDocument(paginator, "Print Report");
                MessageBox.Show("Report Successfull printed");
            }
            Application.Current.MainWindow = Owner;
            this.Hide();
            ((ExpertLoginWindow)Application.Current.MainWindow).txtBoxName.Clear();
            ((ExpertLoginWindow)Application.Current.MainWindow).passBoxPass.Clear();
            Owner.Show();
        }
    }
}
