﻿using Security_Analytical_System.Classes;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Security_Analytical_System.ExpertSide
{
    /// <summary>
    /// Логика взаимодействия для ConclusionWindow.xaml
    /// </summary>
    public partial class ConclusionWindow : Window
    {
        static string conString;
        Expert expert;
        Tuple<decimal, decimal> result;

        List<Conclusion> conclusionList;
        List<Mark> expertMarksList;
        static List<string> tables = new List<string>
        {
            "organizational_measures_marks",
            "hardware_measures_marks",
            "system_measures_marks",
            "application_measures_marks"
        };

        public ConclusionWindow(string con, Expert expert)
        {
            InitializeComponent();
            conString = con;
            this.expert = expert;

            conclusionList = HelperClass.LoadConclusions(conString);
            expertMarksList = new List<Mark>();

            foreach (string table in tables)
                expertMarksList.AddRange(HelperClass.LoadExpertMarks(conString, expert.id, table));

            result = CalculateTotalMark(expertMarksList);

            FillControlElements();
        }

        private void FillControlElements()
        {

            for (int i = 0; i < 3; i++)
                switch (i)
                {
                    case 0:
                        windowGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1.5, GridUnitType.Star) });
                        break;
                    case 1:
                        windowGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1.5, GridUnitType.Star) });
                        break;
                    case 2:
                        windowGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(3, GridUnitType.Star) });
                        break;
                }

            for (int i = 0; i < conclusionList.Count + 3; i++)
                windowGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

            Label markLabel, markResult;
            Separator line;

            windowGrid.Children.Add(
                markLabel = new Label
                {
                    Content = "Assessment result: ",
                    FontWeight = FontWeights.Bold
                }
            );
            Grid.SetRow(markLabel, 0);
            Grid.SetColumn(markLabel, 0);

            windowGrid.Children.Add(
                markResult = new Label
                {
                    Content = result.Item1.ToString(),
                    FontWeight = FontWeights.Bold
                }
            );
            Grid.SetRow(markResult, 0);
            Grid.SetColumn(markResult, 1);
            Grid.SetColumnSpan(markResult, 2);

            Label conclusionLabel, conclusionResult;
            windowGrid.Children.Add(
                conclusionLabel = new Label
                {
                    Content = "Conclusion: ",
                    FontWeight = FontWeights.Bold
                }
            );
            Grid.SetRow(conclusionLabel, 1);
            Grid.SetColumn(conclusionLabel, 0);

            windowGrid.Children.Add(
                conclusionResult = new Label
                {
                    Content = GetConclusion(result.Item1),
                    FontWeight = FontWeights.Bold
                }
            );
            Grid.SetRow(conclusionResult, 1);
            Grid.SetColumn(conclusionResult, 1);
            Grid.SetColumnSpan(conclusionResult, 2);

            windowGrid.Children.Add(line = new Separator());
            Grid.SetRow(line, 2);
            Grid.SetColumn(line, 0);
            Grid.SetColumnSpan(line, 3);


            for (int i = 0; i < conclusionList.Count; i++)
            {
                Label range, level, desc;

                windowGrid.Children.Add(
                    range = new Label
                    {
                        Content = conclusionList[i].conclusion_low_range.ToString() +
                        "-" + conclusionList[i].conclusion_high_range.ToString() + " points"
                    }
                );
                Grid.SetRow(range, i + 3);
                Grid.SetColumn(range, 0);

                windowGrid.Children.Add(
                    level = new Label
                    {
                        Content = conclusionList[i].conclusion_level
                    }
                );
                Grid.SetRow(level, i + 3);
                Grid.SetColumn(level, 1);

                windowGrid.Children.Add(
                    desc = new Label
                    {
                        Content = conclusionList[i].conclusion_desc
                    }
                );
                Grid.SetRow(desc, i + 3);
                Grid.SetColumn(desc, 3);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            WriteResult();
            Application.Current.MainWindow = Owner;
            this.Hide();
            ((ExpertPanelWindow)Application.Current.MainWindow).btnConclusion.IsEnabled = false;
            ((ExpertPanelWindow)Application.Current.MainWindow).btnCreateReport.IsEnabled = true;
            Owner.Show();
        }

        private Tuple<decimal, decimal> CalculateTotalMark(List<Mark> marks)
        {
            decimal total = marks.Sum(item => item.mark);

            return Tuple.Create(total, total * expert.expert_value);
        }

        private string GetConclusion(decimal resultMark)
        {
            Conclusion conclusion = conclusionList.FirstOrDefault(
                        i => i.conclusion_low_range <= resultMark && i.conclusion_high_range >= resultMark
            );
            
            return conclusion.conclusion_level + " - " + conclusion.conclusion_desc;
        }

        private void WriteResult()
        {
            using (OleDbConnection con = new OleDbConnection(conString))
            {
                try
                {
                    con.Open();
                    using (OleDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = "INSERT INTO conclusions_marks" +
                            "([expert_username], [total], [total_with_expert_value], [object_id])" +
                            $"VALUES('{expert.username}', {result.Item1}, {result.Item2}, {expert.object_id})";
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (OleDbException ex)
                {
                    MessageBox.Show($"App encountered next problem: {ex.Message}");
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
            }

            using (OleDbConnection con = new OleDbConnection(conString))
            {
                try
                {
                    con.Open();
                    using (OleDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = $"UPDATE experts SET [has_assessed] = True WHERE id = {expert.id}";
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"App encountered next problem: {ex.Message}");
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
            }

            using (OleDbConnection con = new OleDbConnection(conString))
            {
                List<ConclusionMark> conclusionMarks = HelperClass.LoadConclusionMarks(conString, expert.object_id);
                decimal expert_total_average = conclusionMarks.Average(item => item.total_with_expert_value);

                try
                {
                    con.Open();
                    using (OleDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = $"UPDATE objects_marks SET [total_mark] = {expert_total_average} WHERE object_id = {expert.object_id}";
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"App encountered next problem: {ex.Message}");
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }
    }
}
