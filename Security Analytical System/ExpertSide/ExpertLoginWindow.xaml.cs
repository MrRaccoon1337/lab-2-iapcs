﻿using Security_Analytical_System.Classes;
using Security_Analytical_System.ExpertSide;
using System;
using System.Data.OleDb;
using System.IO;
using System.Windows;
using System.Windows.Input;

namespace Security_Analytical_System
{
    /// <summary>
    /// Логика взаимодействия для ExpertLoginWindow.xaml
    /// </summary>
    public partial class ExpertLoginWindow : Window
    {
        static string conString;
        public ExpertLoginWindow()
        {
            InitializeComponent();
            conString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" + Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName + "\\lab2.accdb";
        }

        private void ButtonLogin_Click(object sender, RoutedEventArgs e)
        {
            string username = txtBoxName.Text;
            string password = Hasher.hashSHA256(passBoxPass.Password.ToString());

            using (OleDbConnection con = new OleDbConnection(conString))
            {
                try
                {
                    con.Open();
                    using (OleDbCommand cmd = new OleDbCommand($"SELECT * FROM experts WHERE username = '{username}'", con))
                    {
                        OleDbDataReader reader = cmd.ExecuteReader();
                        if (reader.Read() && reader["password"].ToString() == password)
                        {
                            if (Convert.ToBoolean(reader["has_assessed"].ToString()))
                                MessageBox.Show("You've already assessed this object!\n " +
                                    "Please contact administrator for futher info");
                            else
                            {
                                ExpertPanelWindow expertPanelWindow = new ExpertPanelWindow(
                                    conString,
                                    new Expert(
                                        reader["username"].ToString(),
                                        reader["password"].ToString(),
                                        Convert.ToDecimal(reader["expert_value"].ToString()),
                                        Convert.ToInt32(reader["object_id"].ToString()),
                                        Convert.ToInt32(reader["id"].ToString())
                                ));
                                expertPanelWindow.Owner = this;
                                Application.Current.MainWindow = expertPanelWindow;
                                this.Hide();
                                expertPanelWindow.Show();
                            }
                        }
                        else
                            MessageBox.Show("Wrong username or password!");
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"App encountered next problem: {ex.Message}");
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                ButtonLogin_Click(sender, e);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.MainWindow = Owner;
            this.Hide();
            Owner.Show();
        }
    }
}
