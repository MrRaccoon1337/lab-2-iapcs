﻿using Security_Analytical_System.Classes;
using Security_Analytical_System.ExpertSide.SafetyMeansAndMeasures.SoftwareAndHardwareMeasuresWindow.ApplicationMeasures;
using Security_Analytical_System.ExpertSide.SafetyMeansAndMeasures.SoftwareAndHardwareMeasuresWindow.HardwareMeasures;
using Security_Analytical_System.ExpertSide.SafetyMeansAndMeasures.SoftwareAndHardwareMeasuresWindow.SystemMeasures;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Security_Analytical_System.ExpertSide.SafetyMeansAndMeasures.SoftwareAndHardwareMeasuresWindow
{
    /// <summary>
    /// Логика взаимодействия для SoftwareAndHardwareMeasuresWindow.xaml
    /// </summary>
    public partial class SoftwareAndHardwareMeasuresWindow : Window
    {
        static string conString;
        Expert expert;
        public SoftwareAndHardwareMeasuresWindow(string con, Expert expert)
        {
            InitializeComponent();
            conString = con;
            this.expert = expert;
        }

        private void btnHardware_Click(object sender, RoutedEventArgs e)
        {
            HardwareMeasuresWindow hardwareMeasuresWidnow = new HardwareMeasuresWindow(conString, expert);
            hardwareMeasuresWidnow.Owner = this;
            Application.Current.MainWindow = hardwareMeasuresWidnow;
            this.Hide();
            hardwareMeasuresWidnow.Show();
        }

        private void btnSystem_Click(object sender, RoutedEventArgs e)
        {
            SystemMeasuresWindow systemMeasuresWindow = new SystemMeasuresWindow(conString, expert);
            systemMeasuresWindow.Owner = this;
            Application.Current.MainWindow = systemMeasuresWindow;
            this.Hide();
            systemMeasuresWindow.Show();
        }

        private void btnApplied_Click(object sender, RoutedEventArgs e)
        {
            ApplicationMeasuresWindow applicationMeasuresWindow = new ApplicationMeasuresWindow(conString, expert);
            applicationMeasuresWindow.Owner = this;
            Application.Current.MainWindow = applicationMeasuresWindow;
            this.Hide();
            applicationMeasuresWindow.Show();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.MainWindow = Owner;
            this.Hide();
            Owner.Show();
        }
    }
}
