﻿using Security_Analytical_System.Classes;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Security_Analytical_System.ExpertSide.SafetyMeansAndMeasures.SoftwareAndHardwareMeasuresWindow.SystemMeasures
{
    /// <summary>
    /// Логика взаимодействия для SystemMeasuresWindow.xaml
    /// </summary>
    public partial class SystemMeasuresWindow : Window
    {
        static string conString;
        Expert expert;
        List<Criteria> criteriaList;
        public SystemMeasuresWindow(string con, Expert expert)
        {
            InitializeComponent();
            conString = con;
            this.expert = expert;

            criteriaList = HelperClass.LoadCriterias(conString, "system_measures");
            FillControlElements();
        }

        private void FillControlElements()
        {
            List<string> cats = criteriaList.Select(o => o.criteria_cat).Distinct().ToList();
            List<string> subcats = criteriaList.Select(o => o.criteria_subcat).Distinct().ToList();
            subcats.AddRange(subcats);

            for (int i = 0; i < 3; i++)
                switch (i)
                {
                    case 0:
                        windowGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(7, GridUnitType.Star) });
                        break;
                    case 1:
                        windowGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(2, GridUnitType.Star) });
                        break;
                    case 2:
                        windowGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                        break;
                }

            for (int i = 0; i < criteriaList.Count + cats.Count + subcats.Count; i++)
                windowGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

            int cat_id = 0;
            int subcat_id = 0;

            for (int i = 0; i < criteriaList.Count; i++)
            {

                if (cat_id < cats.Count)
                    if (criteriaList[i].criteria_cat == cats[cat_id])
                    {
                        Label catLabel;
                        windowGrid.Children.Add(
                            catLabel = new Label
                            {
                                Content = criteriaList[i].criteria_cat,
                                FontWeight = FontWeights.Bold
                            }
                        );
                        Grid.SetRow(catLabel, i + cat_id + subcat_id);
                        Grid.SetColumn(catLabel, 0);
                        Grid.SetColumnSpan(catLabel, 3);
                        cat_id++;
                    }

                if (subcat_id < subcats.Count)
                    if (criteriaList[i].criteria_subcat == subcats[subcat_id])
                    {
                        Label subcatLabel;
                        windowGrid.Children.Add(
                            subcatLabel = new Label
                            {
                                Content = "   " + criteriaList[i].criteria_subcat,
                                FontWeight = FontWeights.Bold
                            }
                        );
                        Grid.SetRow(subcatLabel, i + cat_id + subcat_id);
                        Grid.SetColumn(subcatLabel, 0);
                        Grid.SetColumnSpan(subcatLabel, 3);
                        subcat_id++;
                    }

                TextBlock label, range;
                TextBox txtBox;
                windowGrid.Children.Add(
                    label = new TextBlock
                    {
                        Text = criteriaList[i].criteria_name +
                        (!string.IsNullOrEmpty(criteriaList[i].criteria_desc) ? $" ({criteriaList[i].criteria_desc})" : ""),
                        TextWrapping = TextWrapping.WrapWithOverflow
                    }
                );
                Grid.SetRow(label, i + cat_id + subcat_id);
                Grid.SetColumn(label, 0);
                windowGrid.Children.Add(
                    range = new TextBlock
                    {
                        Text = criteriaList[i].criteria_low_range.ToString() +
                        "-" + criteriaList[i].criteria_high_range.ToString() + " points"
                    }
                );
                Grid.SetRow(range, i + cat_id + subcat_id);
                Grid.SetColumn(range, 1);
                windowGrid.Children.Add(
                    txtBox = new TextBox
                    {
                        Name = $"txtBoxMark{i:D2}",
                        Margin = new Thickness(5)
                    }
                );
                Grid.SetRow(txtBox, i + cat_id + subcat_id);
                Grid.SetColumn(txtBox, 2);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            bool flag = false;
            List<Mark> marks = new List<Mark>();

            foreach (var txtBox in HelperClass.FindVisualChildren<TextBox>(Application.Current.MainWindow))
            {
                if (String.IsNullOrEmpty(txtBox.Text))
                    flag = false;
                else
                {
                    int id = Convert.ToInt32(txtBox.Name[^2..]);
                    int input = Convert.ToInt32(txtBox.Text);
                    if (!(criteriaList[id].criteria_low_range <= input && input <= criteriaList[id].criteria_high_range))
                    {
                        MessageBox.Show($"Mark for criteria {id + 1} is not in range!");
                        flag = false;
                        e.Cancel = true;
                        break;
                    }
                    else
                    {
                        flag = true;
                        marks.Add(new Mark(expert.id, criteriaList[id].id, input));
                    }
                }
            }

            if (flag)
            {
                int result = 0;
                bool errorFlag = false;
                for (int i = 0; i < marks.Count; i++)
                {
                    using (OleDbConnection con = new OleDbConnection(conString))
                    {
                        try
                        {
                            con.Open();
                            using (OleDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = "INSERT INTO system_measures_marks" +
                                    "([expert_id], [criteria_id], [mark])" +
                                    $"VALUES({marks[i].expert_id}, {marks[i].criteria_id}, {marks[i].mark})";
                                result = cmd.ExecuteNonQuery();
                            }
                        }
                        catch (OleDbException ex) when (ex.ErrorCode == -2147467259)
                        {
                            if (!errorFlag)
                                MessageBox.Show($"You've already assesed these measures!");
                            errorFlag = true;
                            Application.Current.MainWindow = Owner;
                            this.Hide();
                            ((SoftwareAndHardwareMeasuresWindow)Application.Current.MainWindow).btnSystem.IsEnabled = false;
                            ((SoftwareAndHardwareMeasuresWindow)Application.Current.MainWindow).btnApplied.IsEnabled = true;
                            Owner.Show();
                        }
                        catch (OleDbException ex)
                        {
                            MessageBox.Show($"App encountered next problem: {ex.Message}");
                        }
                        finally
                        {
                            con.Close();
                            con.Dispose();
                        }
                    }
                }


                if (result != 0)
                {
                    MessageBox.Show("Successfully assesed!");
                    Application.Current.MainWindow = Owner;
                    this.Hide();
                    ((SoftwareAndHardwareMeasuresWindow)Application.Current.MainWindow).btnSystem.IsEnabled = false;
                    ((SoftwareAndHardwareMeasuresWindow)Application.Current.MainWindow).btnApplied.IsEnabled = true;
                    Owner.Show();
                }
            }
            else
            {
                MessageBox.Show("Not all marks were given!");
                e.Cancel = true;
            }
        }
    }
}
