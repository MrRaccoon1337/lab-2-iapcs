﻿using Security_Analytical_System.Classes;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Security_Analytical_System.AdminSide.ObjectForms
{
    /// <summary>
    /// Логика взаимодействия для ObjectAddWindow.xaml
    /// </summary>
    public partial class ObjectAddWindow : Window
    {
        static string conString;
        public ObjectAddWindow(string con)
        {
            InitializeComponent();
            conString = con;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            int objectId = 0;

            using (OleDbConnection con = new OleDbConnection(conString))
            {
                try
                {
                    con.Open();
                    using (OleDbCommand cmd = con.CreateCommand())
                    {
                        Classes.Object newObject = new Classes.Object(txtBoxName.Text.ToString());
                        cmd.Parameters.AddWithValue("@name", newObject.object_name);
                        cmd.CommandText = @"INSERT INTO objects([object_name]) VALUES(@name)";
                        int res = cmd.ExecuteNonQuery();
                        if (res != 0)
                            MessageBox.Show("Object was successfully added!");
                        cmd.CommandText = "SELECT @@Identity";
                        objectId = (int)cmd.ExecuteScalar();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"App encountered next problem: {ex.Message}");
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
            }
            using (OleDbConnection con = new OleDbConnection(conString))
            {
                try
                {
                    con.Open();
                    using (OleDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = $"INSERT INTO objects_marks([object_id], [total_mark]) VALUES({objectId}, 0)";
                        int res = cmd.ExecuteNonQuery();
                        if (res != 0)
                        {
                            Application.Current.MainWindow = Owner;
                            this.Hide();
                            Owner.Show();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"App encountered next problem: {ex.Message}");
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.MainWindow = Owner;
            this.Hide();
            Owner.Show();
        }
    }
}
