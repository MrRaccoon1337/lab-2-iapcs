﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Security_Analytical_System.AdminSide.ObjectForms
{
    /// <summary>
    /// Логика взаимодействия для ObjectRemoveWindow.xaml
    /// </summary>
    public partial class ObjectRemoveWindow : Window
    {
        static string conString;
        public ObjectRemoveWindow(string con)
        {
            InitializeComponent();
            conString = con;
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            using (OleDbConnection con = new OleDbConnection(conString))
            {
                try
                {
                    con.Open();
                    using (OleDbCommand cmd = con.CreateCommand())
                    {
                        cmd.Parameters.AddWithValue("@name", txtBoxName.Text.ToString());
                        cmd.CommandText = @"DELETE FROM objects WHERE object_name = @name";
                        var result = MessageBox.Show("Are you sure you?", "Remove Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                        if (result == MessageBoxResult.Yes)
                        {
                            if (cmd.ExecuteNonQuery() != 0)
                            {
                                MessageBox.Show("Object was successfully removed!");

                                Application.Current.MainWindow = Owner;
                                this.Hide();
                                Owner.Show();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"App encountered next problem: {ex.Message}");
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.MainWindow = Owner;
            this.Hide();
            Owner.Show();
        }
    }
}
