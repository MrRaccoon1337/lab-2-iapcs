﻿using Security_Analytical_System.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Security_Analytical_System.ExpertForms
{
    /// <summary>
    /// Логика взаимодействия для ExpertEditWindow.xaml
    /// </summary>
    public partial class ExpertEditWindow : Window
    {
        static string conString;
        int object_id;
        public ExpertEditWindow(string con, int obj)
        {
            InitializeComponent();
            conString = con;
            object_id = obj;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            using (OleDbConnection con = new OleDbConnection(conString))
            {
                try
                {
                    con.Open();
                    using (OleDbCommand cmd = con.CreateCommand())
                    {
                        string name = txtBoxInputName.Text.ToString();
                        string password = string.IsNullOrEmpty(txtBoxNewPass.Text.ToString()) ? txtBoxOldPass.Text.ToString() : Hasher.hashSHA256(txtBoxNewPass.Text.ToString());
                        cmd.Parameters.AddWithValue("@username", txtBoxName.Text);
                        cmd.Parameters.AddWithValue("@password", password);
                        cmd.Parameters.Add("@expert_value", OdbcType.NVarChar).Value = Convert.ToDecimal(txtBoxValue.Text).ToString("0.00");
                        cmd.CommandText = $@"UPDATE experts SET [username] = @username, [password] = @password, [expert_value] = @expert_value WHERE username = '{name}' AND object_id = {object_id}";
                        int res = cmd.ExecuteNonQuery();
                        if (res != 0)
                        {
                            MessageBox.Show("Expert was successfully edited!");
                            txtBoxOldPass.Text = password;
                            Application.Current.MainWindow = Owner;
                            this.Hide();
                            Owner.Show();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"App encountered next problem: {ex.Message}");
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }

        private void txtBoxId_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtBoxInputName.Text.ToString()))
            {
                using (OleDbConnection con = new OleDbConnection(conString))
                {
                    try
                    {
                        con.Open();
                        string name = txtBoxInputName.Text.ToString();
                        using (OleDbCommand cmd = new OleDbCommand($"SELECT * FROM experts WHERE username LIKE '%{name}%' AND object_id = {object_id}", con))
                        {
                            OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.SingleRow);
                            if (reader.Read())
                            {
                                txtBoxName.Text = reader["username"].ToString();
                                txtBoxOldPass.Text = reader["password"].ToString();
                                txtBoxNewPass.Text = string.Empty;
                                txtBoxValue.Text = reader["expert_value"].ToString();

                                txtBoxName.IsEnabled = true;
                                txtBoxNewPass.IsEnabled = true;
                                txtBoxValue.IsEnabled = true;
                                btnSave.IsEnabled = true;
                            }
                            reader.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"App encountered next problem: {ex.Message}");
                    }
                    finally
                    {
                        con.Close();
                        con.Dispose();
                    }
                }
            }
            else 
            {
                txtBoxName.Clear();
                txtBoxOldPass.Clear();
                txtBoxNewPass.Clear();
                txtBoxValue.Clear();

                txtBoxName.IsEnabled = false;
                txtBoxNewPass.IsEnabled = false;
                txtBoxValue.IsEnabled = false;
                btnSave.IsEnabled = false;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.MainWindow = Owner;
            this.Hide();
            Owner.Show();
        }
    }
}
