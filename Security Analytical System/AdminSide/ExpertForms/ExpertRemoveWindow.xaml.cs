﻿using System;
using System.Data.OleDb;
using System.Windows;

namespace Security_Analytical_System.ExpertForms
{
    /// <summary>
    /// Логика взаимодействия для ExpertRemoveWindow.xaml
    /// </summary>
    public partial class ExpertRemoveWindow : Window
    {
        static string conString;
        int object_id;
        public ExpertRemoveWindow(string con, int obj)
        {
            InitializeComponent();
            conString = con;
            object_id = obj;
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            using (OleDbConnection con = new OleDbConnection(conString))
            {
                try
                {
                    con.Open();
                    using (OleDbCommand cmd = con.CreateCommand())
                    {
                        string name = txtBoxName.Text.ToString();
                        cmd.Parameters.AddWithValue("@name", name);
                        cmd.Parameters.AddWithValue("@object_id", object_id);
                        cmd.CommandText = @"DELETE FROM experts WHERE username = @name AND object_id = @object_id";
                        var result = MessageBox.Show("Are you sure you?", "Remove Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                        if (result == MessageBoxResult.Yes)
                            cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"App encountered next problem: {ex.Message}");
                }
                finally
                {
                    con.Close();
                    con.Dispose();

                    MessageBox.Show("Expert was successfully removed!");

                    Application.Current.MainWindow = Owner;
                    this.Hide();
                    Owner.Show();
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.MainWindow = Owner;
            this.Hide();
            Owner.Show();
        }
    }
}
