﻿using System;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Windows;
using Security_Analytical_System.Classes;

namespace Security_Analytical_System.ExpertForms
{
    /// <summary>
    /// Логика взаимодействия для ExpertAddWindow.xaml
    /// </summary>
    public partial class ExpertAddWindow : Window
    {
        static string conString;
        int object_id;
        public ExpertAddWindow(string con, int obj)
        {
            InitializeComponent();
            conString = con;
            object_id = obj;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            using (OleDbConnection con = new OleDbConnection(conString))
            {
                try
                {
                    con.Open();
                    using (OleDbCommand cmd = con.CreateCommand())
                    {
                        Expert newExpert = new Expert(
                            txtBoxName.Text.ToString(),
                            passBoxPass.Password.ToString(),
                            Convert.ToDecimal(txtBoxValue.Text.ToString()),
                            object_id
                       );
                        cmd.Parameters.AddWithValue("@username", newExpert.username);
                        cmd.Parameters.AddWithValue("@password", newExpert.password);
                        cmd.Parameters.Add("@expert_value", OdbcType.NVarChar).Value =  newExpert.expert_value.ToString("0.00");
                        cmd.Parameters.AddWithValue("@object_id", newExpert.object_id);
                        cmd.CommandText = @"INSERT INTO experts" +
                            "([username], [password], [expert_value], [object_id])" +
                            "VALUES(@username, @password, @expert_value, @object_id)";
                        if (cmd.ExecuteNonQuery() != 0)
                        {
                            MessageBox.Show("Expert was successfully added!");
                            Application.Current.MainWindow = Owner;
                            this.Hide();
                            Owner.Show();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"App encountered next problem: {ex.Message}");
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }


        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.MainWindow = Owner;
            this.Hide();
            Owner.Show();
        }
    }
}
