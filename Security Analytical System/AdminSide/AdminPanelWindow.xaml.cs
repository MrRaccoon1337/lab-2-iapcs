﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Security_Analytical_System.Classes;
using Security_Analytical_System.ExpertForms;
using Security_Analytical_System.AdminSide.ObjectForms;

namespace Security_Analytical_System
{
    /// <summary>
    /// Логика взаимодействия для AdminPanelWindow.xaml
    /// </summary>
    public partial class AdminPanelWindow : Window
    {
        string conString;
        int selectedObjectId = 1;
        int selectedObjectIndex = 0;

        public AdminPanelWindow()
        {
            InitializeComponent();
            conString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" + Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName + "\\lab2.accdb";
        }


        private DataView LoadExperts()
        {
            using (OleDbConnection con = new OleDbConnection(conString))
            {
                DataTable expertDataTable = new DataTable("experts");
                try
                {
                    con.Open();
                    using (OleDbCommand cmd = new OleDbCommand($"SELECT * FROM experts WHERE object_id = {selectedObjectId}", con))
                    {
                        OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                        da.Fill(expertDataTable);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"App encountered next problem: {ex.Message}");
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
                return expertDataTable.DefaultView;
            }
        }

        private List<Classes.Object> LoadObjects()
        {
            using (OleDbConnection con = new OleDbConnection(conString))
            {
                List<Classes.Object> list = new List<Classes.Object>();
                try
                {
                    con.Open();
                    using (OleDbCommand cmd = new OleDbCommand($"SELECT * FROM objects", con))
                    {
                        OleDbDataReader dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            Classes.Object obj = new Classes.Object();
                            obj.id = Convert.ToInt32(dr["id"].ToString());
                            obj.object_name = dr["object_name"].ToString();
                            list.Add(obj);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"App encountered next problem: {ex.Message}");
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
                return list;
            }
        }

        private void btnAddExpert_Click(object sender, RoutedEventArgs e)
        {
            ExpertAddWindow expertAddWindow = new ExpertAddWindow(conString, selectedObjectId);
            expertAddWindow.Owner = this;
            Application.Current.MainWindow = expertAddWindow;
            this.Hide();
            expertAddWindow.Show();
        }

        private void btnAddObject_Click(object sender, RoutedEventArgs e)
        {
            ObjectAddWindow objAddWindow = new ObjectAddWindow(conString);
            objAddWindow.Owner = this;
            Application.Current.MainWindow = objAddWindow;
            this.Hide();
            objAddWindow.Show();
        }

        private void btnRemoveObject_Click(object sender, RoutedEventArgs e)
        {
            ObjectRemoveWindow objRemoveWindow = new ObjectRemoveWindow(conString);
            objRemoveWindow.Owner = this;
            Application.Current.MainWindow = objRemoveWindow;
            this.Hide();
            objRemoveWindow.Show();
        }

        private void btnRemoveExpert_Click(object sender, RoutedEventArgs e)
        {
            ExpertRemoveWindow expertRemoveWindow = new ExpertRemoveWindow(conString, selectedObjectId);
            expertRemoveWindow.Owner = this;
            Application.Current.MainWindow = expertRemoveWindow;
            this.Hide();
            expertRemoveWindow.Show();
        }

        private void Window_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (IsVisible)
            {
                dtgridExperts.ItemsSource = null;
                dtgridExperts.ItemsSource = LoadExperts();

                dtgridScores.ItemsSource = null;
                dtgridScores.ItemsSource = HelperClass.LoadConclusionMarks(conString, selectedObjectId);

                lblTotalScore.Content = HelperClass.LoadObjectMark(conString, selectedObjectId);

                lstviewEvalObjects.ItemsSource = null;
                lstviewEvalObjects.ItemsSource = LoadObjects();
                lstviewEvalObjects.SelectedIndex = selectedObjectIndex;
            }
        }


        private void btnEditExpert_Click(object sender, RoutedEventArgs e)
        {
            ExpertEditWindow expertEditWindow = new ExpertEditWindow(conString, selectedObjectId);
            expertEditWindow.Owner = this;
            Application.Current.MainWindow = expertEditWindow;
            this.Hide();
            expertEditWindow.Show();
        }

        private void lstviewEvalObjects_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedObjectIndex = lstviewEvalObjects.SelectedIndex;
            dynamic selectedItem = lstviewEvalObjects.SelectedItem;
            if (selectedItem != null)
                selectedObjectId = Convert.ToInt32(selectedItem.id);
            else
                selectedObjectId = 1;

            lblTotalScore.Content = HelperClass.LoadObjectMark(conString, selectedObjectId).total_mark.ToString();

            dtgridScores.ItemsSource = null;
            dtgridScores.ItemsSource = HelperClass.LoadConclusionMarks(conString, selectedObjectId);

            dtgridExperts.ItemsSource = null;
            dtgridExperts.ItemsSource = LoadExperts();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.MainWindow = Owner;
            this.Hide();
            ((AdminLoginWindow)Application.Current.MainWindow).txtBoxName.Clear();
            ((AdminLoginWindow)Application.Current.MainWindow).passBoxPass.Clear();
            Owner.Show();
        }
    }
}
