﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Data.OleDb;
using Security_Analytical_System.Classes;

namespace Security_Analytical_System
{
    /// <summary>
    /// Логика взаимодействия для AdminLoginWindow.xaml
    /// </summary>
    public partial class AdminLoginWindow : Window
    {
        string conString;

        public AdminLoginWindow()
        {
            InitializeComponent();
            conString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" + Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName + "\\lab2.accdb";
        }

        private void ButtonLogin_Click(object sender, RoutedEventArgs e)
        {
            string username = txtBoxName.Text;
            string password = Hasher.hashSHA256(passBoxPass.Password.ToString());

            using (OleDbConnection con = new OleDbConnection(conString))
            {
                try
                {
                    con.Open();
                    using (OleDbCommand cmd = new OleDbCommand($"SELECT * FROM admins WHERE username = '{username}'", con))
                    {
                        OleDbDataReader reader = cmd.ExecuteReader();
                        if (reader.Read() && reader["password"].ToString() == password)
                        {
                            AdminPanelWindow adminPanelWindow = new AdminPanelWindow();
                            adminPanelWindow.Owner = this;
                            Application.Current.MainWindow = adminPanelWindow;
                            this.Hide();
                            adminPanelWindow.Show();
                        }
                        else
                            MessageBox.Show("Wrong username or password!");
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"App encountered next problem: {ex.Message}");
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
                ButtonLogin_Click(sender, e);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.MainWindow = Owner;
            this.Hide();
            Owner.Show();
        }
    }
}
