﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Security_Analytical_System.Classes
{
    public class Criteria
    {
        public int id { get; set; }
        public string criteria_name { get; set; }
        public string criteria_desc { get; set; }
        public string criteria_cat { get; set; }
        public string criteria_subcat { get; set; }
        public int criteria_low_range { get; set; }
        public int criteria_high_range { get; set; }
        public Criteria() { }
        public Criteria(string id, string criteria_name, string criteria_desc, string criteria_cat, string criteria_subcat, string range)
        {
            this.id = Convert.ToInt32(id);
            this.criteria_name = criteria_name;
            this.criteria_desc = criteria_desc;
            this.criteria_cat = criteria_cat;
            this.criteria_subcat = criteria_subcat;

            string[] limits = Regex.Split(range, @"\D+");
            this.criteria_low_range = Convert.ToInt32(limits[0]);
            this.criteria_high_range = Convert.ToInt32(limits[1]);
        }
    }
}
