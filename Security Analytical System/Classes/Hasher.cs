﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Security_Analytical_System.Classes
{
    public static class Hasher
    {
        public static string hashSHA256(string str)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(str);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte b in hash)
                hashString += String.Format("{0:x2}", b);
            hashstring.Dispose();
            return hashString;
        }
    }
}
