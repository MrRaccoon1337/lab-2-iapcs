﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Security_Analytical_System.Classes
{
    public class Object
    {
        public Object() { }
        public Object(string object_name)
        {
            this.object_name = object_name;
        }

        public int id { get; set; }
        public string object_name { get; set; }
    }
}
