﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace Security_Analytical_System.Classes
{
    public static class HelperClass
    {
        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj == null)
                yield break;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                var child = VisualTreeHelper.GetChild(depObj, i);

                if (child != null && child is T)
                    yield return (T)child;

                foreach (T childOfChild in FindVisualChildren<T>(child))
                    yield return childOfChild;
            }
        }

        public static List<Criteria> LoadCriterias(string conString, string table)
        {
            using (OleDbConnection con = new OleDbConnection(conString))
            {
                List<Criteria> list = new List<Criteria>();
                try
                {
                    con.Open();
                    using (OleDbCommand cmd = new OleDbCommand($"SELECT * FROM {table}", con))
                    {
                        OleDbDataReader dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            Criteria criteria = new Criteria(
                                dr["id"].ToString(),
                                dr["criteria_name"].ToString(),
                                dr["criteria_desc"].ToString(),
                                dr["criteria_cat"].ToString(),
                                dr["criteria_subcat"].ToString(),
                                dr["range"].ToString()
                            );
                            list.Add(criteria);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"App encountered next problem: {ex.Message}");
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
                return list;
            }
        }

        public static List<Conclusion> LoadConclusions(string conString)
        {
            using (OleDbConnection con = new OleDbConnection(conString))
            {
                List<Conclusion> list = new List<Conclusion>();
                try
                {
                    con.Open();
                    using (OleDbCommand cmd = new OleDbCommand($"SELECT * FROM conclusions", con))
                    {
                        OleDbDataReader dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            Conclusion conclusion = new Conclusion(
                                dr["id"].ToString(),
                                dr["conclusion_level"].ToString(),
                                dr["conclusion_desc"].ToString(),
                                dr["range"].ToString()
                            );
                            list.Add(conclusion);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"App encountered next problem: {ex.Message}");
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
                return list;
            }
        }

        public static List<Mark> LoadExpertMarks(string conString, int expertId, string table)
        {
            using (OleDbConnection con = new OleDbConnection(conString))
            {
                List<Mark> list = new List<Mark>();
                try
                {
                    con.Open();
                    using (OleDbCommand cmd = new OleDbCommand($"SELECT * FROM {table} WHERE expert_id = {expertId}", con))
                    {
                        OleDbDataReader dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            Mark mark = new Mark(
                                Convert.ToInt32(dr["expert_id"].ToString()),
                                Convert.ToInt32(dr["criteria_id"].ToString()),
                                Convert.ToInt32(dr["mark"].ToString()),
                                Convert.ToInt32(dr["id"].ToString())
                            );
                            list.Add(mark);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"App encountered next problem: {ex.Message}");
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
                return list;
            }
        }

        public static ConclusionMark LoadExpertConclusionMark(string conString, string expertUsername)
        {
            ConclusionMark conclusionMark = new ConclusionMark();
            using (OleDbConnection con = new OleDbConnection(conString))
            {
                try
                {
                    con.Open();
                    using (OleDbCommand cmd = new OleDbCommand($"SELECT * FROM conclusions_marks WHERE expert_username = '{expertUsername}'", con))
                    {
                        OleDbDataReader dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            conclusionMark = new ConclusionMark(
                                dr["expert_username"].ToString(),
                                Convert.ToDecimal(dr["total"].ToString()),
                                Convert.ToDecimal(dr["total_with_expert_value"].ToString()),
                                Convert.ToInt32(dr["object_id"].ToString()),
                                Convert.ToInt32(dr["id"].ToString())
                            );
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"App encountered next problem: {ex.Message}");
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
                return conclusionMark;
            }
        }
        
        public static List<ConclusionMark> LoadConclusionMarks(string conString, int objectId)
        {
            List<ConclusionMark> list = new List<ConclusionMark>();
            using (OleDbConnection con = new OleDbConnection(conString))
            {
                try
                {
                    con.Open();
                    using (OleDbCommand cmd = new OleDbCommand($"SELECT * FROM conclusions_marks WHERE object_id = {objectId}", con))
                    {
                        OleDbDataReader dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            ConclusionMark conclusionMark = new ConclusionMark(
                                dr["expert_username"].ToString(),
                                Convert.ToDecimal(dr["total"].ToString()),
                                Convert.ToDecimal(dr["total_with_expert_value"].ToString()),
                                Convert.ToInt32(dr["object_id"].ToString()),
                                Convert.ToInt32(dr["id"].ToString())
                            );
                            list.Add(conclusionMark);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"App encountered next problem: {ex.Message}");
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
                return list;
            }
        }

        public static ObjectMark LoadObjectMark(string conString, int objectId)
        {
            ObjectMark objectMark = new ObjectMark();
            using (OleDbConnection con = new OleDbConnection(conString))
            {
                try
                {
                    con.Open();
                    using (OleDbCommand cmd = new OleDbCommand($"SELECT * FROM objects_marks WHERE object_id = {objectId}", con))
                    {
                        OleDbDataReader dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            objectMark = new ObjectMark(
                                Convert.ToInt32(dr["object_id"].ToString()),
                                Convert.ToDecimal(dr["total_mark"].ToString())
                            );
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"App encountered next problem: {ex.Message}");
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
                return objectMark;
            }
        }
    }
}
