﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Security_Analytical_System.Classes
{
    public class ConclusionMark
    {
        public int id { get; set; }
        public string expert_username { get; set; }
        public decimal total { get; set; }
        public decimal total_with_expert_value { get; set; }
        public int object_id { get; set; }
        public ConclusionMark() { }
        public ConclusionMark(string expert_username, decimal total, decimal total_with_expert_value, int object_id, int id = 0)
        {
            this.id = id;
            this.expert_username = expert_username;
            this.total = total;
            this.total_with_expert_value = total_with_expert_value;
            this.object_id = object_id;
        }
    }
}
