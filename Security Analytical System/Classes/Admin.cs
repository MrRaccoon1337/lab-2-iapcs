﻿namespace Security_Analytical_System.Classes
{
    public class Admin
    {
        public int id { get; set; }
        public string username { get; set; }
        public string password
        {
            get { return password; }
            set { password = Hasher.hashSHA256(value); }
        }
    }
}
