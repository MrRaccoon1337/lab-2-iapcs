﻿namespace Security_Analytical_System.Classes
{
    public class Expert
    {
        public Expert(string username, string password, decimal expert_value, int object_id, int id = 0)
        {
            this.id = id;
            this.username = username;
            this.password = Hasher.hashSHA256(password);
            this.expert_value = expert_value;
            this.object_id = object_id;
        }

        public int id { get; }
        public string username { get; set; }
        public string password { get; set; }
        public decimal expert_value { get; set; }
        public int object_id { get; set; }
    }
}
