﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Security_Analytical_System.Classes
{
    public class ObjectMark
    {
        int id { get; set; }
        public int object_id { get; set; }
        public decimal total_mark { get; set; }
        public ObjectMark() { }
        public ObjectMark(int object_id, decimal total_mark, int id = 0)
        {
            this.id = id;
            this.object_id = object_id;
            this.total_mark = total_mark;
        }
    }
}
