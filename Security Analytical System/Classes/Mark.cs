﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Security_Analytical_System.Classes
{
    public class Mark
    {
        public int id { get; set; }
        public int expert_id { get; set; }
        public int criteria_id { get; set; }
        public int mark{ get; set; }
        public Mark() { }
        public Mark(int expert_id, int criteria_id, int mark, int id = 0)
        {
            this.id = id;
            this.expert_id = expert_id;
            this.criteria_id = criteria_id;
            this.mark = mark;
        }
    }
}
