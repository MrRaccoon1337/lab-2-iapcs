﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Security_Analytical_System.Classes
{
    public class Conclusion
    {
        public int id { get; set; }
        public string conclusion_level { get; set; }
        public string conclusion_desc { get; set; }
        public int conclusion_low_range { get; set; }
        public int conclusion_high_range { get; set; }
        public Conclusion() { }
        public Conclusion(string id, string conclusion_level, string conclusion_desc, string range)
        {
            this.id = Convert.ToInt32(id);
            this.conclusion_level = conclusion_level;
            this.conclusion_desc = conclusion_desc;

            string[] limits = Regex.Split(range, @"\D+");
            this.conclusion_low_range = Convert.ToInt32(limits[0]);
            this.conclusion_high_range = Convert.ToInt32(limits[1]);
        }
    }
}
